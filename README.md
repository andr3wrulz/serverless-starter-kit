# Game Share

## Description
This project is intended to be a replacement for the previous iteration of my video game key sharing site that used AWS AppSync and Vue. It also served as the basis for a Serverless Static Site and API in a box framework as the pattern seemed useful. The general idea is for the static content to be served via S3 and CloudFront with an API backed by API Gateway and Lambda for increased flexibility.

## Features
- Automated deployments of frontend and backend
- Linting and static code scanning
- Dynamic feature environments - every branch gets a production representative environment
- Gitlab environment integration including urls for quick access
- Low cost architecture (base ~$0.50/month + domain registration, scales with traffic)

## Architecture
All single instance resources are created in the setup stack (`setup/setup.yml`). This includes resources needed by the pipeline as well as anything not suitable to be deployed once for every branch.

Each branch in the repository will deploy an instance of a stack (`templates/stack.yml`) containing an API stage and lambda to back the API. Which lambda to execute is controlled via a stage variable. Each branch also gets a copy of the frontend hosted in the web bucket under a separate prefix.

![Architecture diagram](docs/architecture.jpg)

How the API and frontend are accessed is based on the environment (branch) it corresponds to. 

![Access diagram](docs/access.jpg)

## Frontend
Currently the frontend is just a sample react project but the process should be compatible with any static site framework (you may need to change the build process a little bit in the `.gitlab-ci.yml`).

Routing for different environments/branches is handled via cache behaviours on the CloudFront distribution. High level, production code goes in the `/production` prefix, development code goes in the `/development` and anything else goes in `/feature/BRANCH_NAME`. The CloudFront distribution has three origins, one for each root prefix, and since the production one is the default one, anything that doesn't look like a development or feature url will route to the production prefix. This may lead to a gotcha down the road if your project uses "development" or "feature" as production paths but that seems silly.

The frontend app needs to be configured to serve paths relative to index.html in order to support the non-production urls. This can be done with the `"homepage": "."` setting in `package.json` ([source](https://create-react-app.dev/docs/deployment/#serving-the-same-build-from-different-paths)). This is already done in the sample react app included in this repo.

## API
The entire API is served by a single proxied lambda function (`lambda/api/index.py`). This is probably fine for smaller projects but may need to be redesigned for larger ones. Since every API endpoint will route to the single function, the handler for the function should include logic to determine the endpoint that was called which is included in the event(`event['path']`) along with [a bunch](https://docs.aws.amazon.com/apigateway/latest/developerguide/http-api-develop-integrations-lambda.html) of other info about the request.

**Note:** _This framework/skeleton/starter does not handle any authentication for API requests. Any production usage of this project should handle its own authentication._
{: .alert .alert-warning}

## Setup
1. Register a domain in Route53
1. Deploy the `setup/setup.yml` template in CloudFormation, must be deployed in us-east-1 (unless you pull the ACM cert out and do that manually as CloudFront certs must live in us-east-1)
1. While the setup stack is deploying, go to the ACM console and add the DNS records for the verification to Route53 (there should be a handy button)
1. Create access keys for the pipeline user in the IAM console
1. Insert access keys into environment variables in the gitlab project `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_DEFAULT_REGION`
1. Update the variables in the `.gitlab-ci.yml` to point to your buckets, api id, and app name
