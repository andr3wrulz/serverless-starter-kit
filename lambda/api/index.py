"""
API Handler Lambda
This lambda function will handle all methods of the API.
"""

import json

def lambda_handler(event, context): # pylint: disable=unused-argument, missing-function-docstring
    # Since we are proxying the entire API to this function, we must utilize event.path to determine our actions
    print(json.dumps(event))
    return {
        'body': json.dumps(event),
        'headers': {
            'Content-Type': 'application/json'
        },
        'statusCode': 200
    }
