AWSTemplateFormatVersion: "2010-09-09"

Description: Stack containing resources used by the pipeline or resources that should be re-used between environments

# Notes:
# - For some ungodly reason, ACM certificates for use with CloudFront MUST be in us-east-1 so this stack is intended
#   to be launched in that region only
# - While deploying, you MUST go to the ACM console and add the verification records to route53
# - Access keys for the pipeline user need to be created and put in gitlab env vars

Parameters:
  pDomainName:
    Type: String
    Default: gameshareapp.com
  
  pAppName:
    Type: String
    Default: game-share

Resources:
  PipelineUser:
    Type: AWS::IAM::User
    Properties:
      UserName: !Sub '${pAppName}-pipeline-user'
      ManagedPolicyArns:
        - !Ref PipelineUserPolicy

  PipelineUserPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      ManagedPolicyName: !Sub '${pAppName}-pipeline-user-policy'
      Description: !Sub 'Policy to be used by the ${pAppName} pipeline user'
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Sid: AllowScopedS3Actions
            Effect: Allow
            Action:
              - s3:*
            Resource:
              - !GetAtt WebsiteBucket.Arn
              - !Sub '${WebsiteBucket.Arn}/*'
              - !GetAtt LambdaBucket.Arn
              - !Sub '${LambdaBucket.Arn}/*'
              - !Sub 'arn:${AWS::Partition}:s3:::cf-templates-*-${AWS::Region}/*'
          - Sid: AllowUnscopedCFActions
            Effect: Allow
            Action:
              - cloudformation:ValidateTemplate
              - cloudformation:ListStacks
            Resource:
              - '*'
          - Sid: AllowScopedCFActions
            Effect: Allow
            Action:
              - cloudformation:*
            Resource:
              - !Sub 'arn:${AWS::Partition}:cloudformation:${AWS::Region}:${AWS::AccountId}:stack/${pAppName}*'
          - Sid: AllowScopedIAMActions
            Effect: Allow
            Action:
              - iam:*
            Resource:
              - !Sub 'arn:${AWS::Partition}:iam::${AWS::AccountId}:role/${pAppName}*'
          - Sid: AllowScopedLambdaActions
            Effect: Allow
            Action:
              - lambda:*
            Resource:
              - !Sub 'arn:${AWS::Partition}:lambda:${AWS::Region}:${AWS::AccountId}:function:${pAppName}*'
          - Sid: AllowScopedApiGWActions
            Effect: Allow
            Action:
              - apigateway:*
            Resource:
              - !Sub 'arn:${AWS::Partition}:apigateway:${AWS::Region}::/restapis/${RestApi}'
              # Other resources are nested in the arn https://docs.aws.amazon.com/apigateway/latest/developerguide/arn-format-reference.html#apigateway-v1-arns
              - !Sub 'arn:${AWS::Partition}:apigateway:${AWS::Region}::/restapis/${RestApi}/*'

  WebsiteBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub '${pAppName}-web'
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: AES256
      LifecycleConfiguration:
        Rules:
          - Id: DeleteFeatureEnvs
            Prefix: feature
            Status: Enabled
            ExpirationInDays: 7
      LoggingConfiguration:
        DestinationBucketName: !Ref LoggingBucket
        LogFilePrefix: web-bucket-access/

  WebsiteBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket: !Ref WebsiteBucket
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Action:
              - s3:GetObject
            Resource: !Sub '${WebsiteBucket.Arn}/*'
            Principal:
              CanonicalUser: !GetAtt CloudFrontOriginAccessIdentity.S3CanonicalUserId
          - Effect: Allow
            Action:
              - s3:GetObject
              - s3:PutObject
              - s3:DeleteObject
            Resource: !Sub '${WebsiteBucket.Arn}/*'
            Principal: 
              AWS:
                - !GetAtt PipelineUser.Arn

  CertificateManagerCertificate:
    Type: AWS::CertificateManager::Certificate
    Properties:
      DomainName: !Ref pDomainName
      SubjectAlternativeNames:
        - !Sub '*.${pDomainName}' # Wildcard cert for any subdomains you want to create later
      ValidationMethod: DNS

  CloudFrontOriginAccessIdentity:
    Type: AWS::CloudFront::CloudFrontOriginAccessIdentity
    Properties:
      CloudFrontOriginAccessIdentityConfig:
        Comment: !Sub 'CloudFront OAI for ${pDomainName}'

  # The usage of multiple origins here is a little confusing but we are utilizing it to hijack requests for thr root
  # of the domain to route them to the production prefix of the bucket. We have to create rules for the other root
  # prefixes so that they can get routed properly. This avoids us having to store the prod files in the root of the
  # bucket which makes updating the frontend code much cleaner.
  # Also, the weird cache policies ids are the AWS managed ones from here: https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/using-managed-cache-policies.html
  CloudFrontDistribution:
    Type: AWS::CloudFront::Distribution
    Properties:
      DistributionConfig:
        Aliases:
          - !Ref pDomainName
        CacheBehaviors: # Rules for specific path patterns to route to origins
          - PathPattern: /development*
            TargetOriginId: development
            ViewerProtocolPolicy: redirect-to-https
            CachePolicyId: 4135ea2d-6df8-44a3-9df3-4b5a84be39ad # Cache disabled
            Compress: true
          - PathPattern: /feature*
            TargetOriginId: feature
            ViewerProtocolPolicy: redirect-to-https
            CachePolicyId: 4135ea2d-6df8-44a3-9df3-4b5a84be39ad # Cache disabled
            Compress: true
        DefaultCacheBehavior:
          Compress: true
          TargetOriginId: production
          ViewerProtocolPolicy: redirect-to-https
          CachePolicyId: 658327ea-f89d-4fab-a63d-7e88639e58f6 # Cache for 24 hours
        DefaultRootObject: index.html
        CustomErrorResponses: # Modify these rules to suit your use cases
          # - ErrorCachingMinTTL: 300 # Redirect any unauth errors back to the home page
          #   ErrorCode: 403
          #   ResponseCode: 200
          #   ResponsePagePath: /index.html
          - ErrorCachingMinTTL: 10 # Redirect any 404 errors to the error page
            ErrorCode: 404
            ResponseCode: 404
            ResponsePagePath: /error.html
        Enabled: true
        HttpVersion: http2
        Logging:
          Bucket: !GetAtt LoggingBucket.DomainName
          IncludeCookies: 'false'
          Prefix: cloudfront-access/
        Origins:
          - DomainName: !Sub '${WebsiteBucket}.s3.amazonaws.com'
            Id: production
            OriginPath: /production
            S3OriginConfig:
              OriginAccessIdentity: !Sub 'origin-access-identity/cloudfront/${CloudFrontOriginAccessIdentity}'
          - DomainName: !Sub '${WebsiteBucket}.s3.amazonaws.com'
            Id: development
            # We don't need to include an origin path as the cache rule will include the prefix
            S3OriginConfig:
              OriginAccessIdentity: !Sub 'origin-access-identity/cloudfront/${CloudFrontOriginAccessIdentity}'
          - DomainName: !Sub '${WebsiteBucket}.s3.amazonaws.com'
            Id: feature
            # We don't need to include an origin path as the cache rule will include the prefix
            S3OriginConfig:
              OriginAccessIdentity: !Sub 'origin-access-identity/cloudfront/${CloudFrontOriginAccessIdentity}'
        PriceClass: PriceClass_100 # Keep it to the cheap endpoints
        ViewerCertificate:
          AcmCertificateArn: !Ref CertificateManagerCertificate
          MinimumProtocolVersion: TLSv1
          SslSupportMethod: sni-only

  WebsiteDns:
    Type: AWS::Route53::RecordSetGroup
    Properties:
      HostedZoneName: !Sub '${pDomainName}.'
      RecordSets:
        - Name: !Ref pDomainName
          Type: A
          AliasTarget:
            DNSName: !GetAtt CloudFrontDistribution.DomainName
            EvaluateTargetHealth: false
            HostedZoneId: Z2FDTNDATAQYW2 # CloudFront
        - Name: !Sub 'www.${pDomainName}'
          Type: A
          AliasTarget:
            DNSName: !GetAtt CloudFrontDistribution.DomainName
            EvaluateTargetHealth: false
            HostedZoneId: Z2FDTNDATAQYW2 # CloudFront

  LambdaBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub '${pAppName}-lambdas'
      LifecycleConfiguration:
        Rules:
          - Id: DeleteFeatureEnvs
            Prefix: feature
            Status: Enabled
            ExpirationInDays: 7

  LoggingBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub '${pAppName}-logging'
      LifecycleConfiguration:
        Rules:
          - Id: DontKeepOver90Days
            Status: Enabled
            ExpirationInDays: 90

  LoggingBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket: !Ref LoggingBucket
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Sid: AllowS3LoggingService
            Effect: Allow
            Principal:
              Service: logging.s3.amazonaws.com
            Action:
              - s3:PutObject
            Resource: !Sub '${LoggingBucket.Arn}/*'

# For the API resources below, we actually create the resource and method once here in the setup template
# Each environment stack will create a stage with a variable so that the environment's lambda gets called
  RestApi:
    Type: AWS::ApiGateway::RestApi
    Properties:
      Name: !Sub '${pAppName}-api'

  ApiExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub '${pAppName}-api-exec-default'
      Policies:
        - PolicyName: LambdaUsage
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Effect: Allow
                Action:
                  - lambda:*
                Resource: !Sub 'arn:${AWS::Partition}:lambda:${AWS::Region}:${AWS::AccountId}:function:${pAppName}-api-*'
      AssumeRolePolicyDocument: # Let API GW assume the role
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service: apigateway.amazonaws.com
            Action: sts:AssumeRole

  ApiResource:
    Type: AWS::ApiGateway::Resource
    Properties:
      RestApiId: !Ref RestApi
      ParentId: !GetAtt RestApi.RootResourceId
      PathPart: '{path+}'

  ApiMethod:
    Type: AWS::ApiGateway::Method
    # DependsOn:
    #   - ApiStage # We need to wait for the stage so the variable evals to a valid lambda
    Properties:
      HttpMethod: ANY
      ResourceId: !Ref ApiResource
      RestApiId: !Ref RestApi
      AuthorizationType: NONE
      Integration:
        Credentials: !GetAtt ApiExecutionRole.Arn
        IntegrationHttpMethod: POST
        Type: AWS_PROXY
        # Ref: https://docs.aws.amazon.com/apigateway/latest/developerguide/aws-api-gateway-stage-variables-reference.html#stage-variables-in-integration-aws-uris
        Uri: !Sub >- # Note that the function name isn't a sub, it's a stage variable
          arn:${AWS::Partition}:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/arn:${AWS::Partition}:lambda:${AWS::Region}:${AWS::AccountId}:function:${!stageVariables.API_LAMBDA_NAME}/invocations

Outputs:
  S3WebsiteURL:
    Value: !GetAtt WebsiteBucket.WebsiteURL
  ApiId:
    Value: !Ref RestApi
  ApiRootResourceId:
    Value: !GetAtt RestApi.RootResourceId
  ApiExecutionUrl:
    Value: !Sub 'https://${RestApi}.execute-api.${AWS::Region}.amazonaws.com'